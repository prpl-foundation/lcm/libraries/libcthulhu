/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__CTHULHU_ERROR_H__)
#define __CTHULHU_ERROR_H__

#ifdef __cplusplus
extern "C"
{
#endif

//    If the operation was successful, the fault code is 0.
//    If the device cannot complete the operation for some unknown reason, it SHOULD reject the operation with a 9001 (Request Denied) fault code.
//    If the device detects the presence of the "userinfo" component in the file source URL, it SHOULD reject the operation with a 9003 (Invalid Arguments) fault code.
//    If the device cannot find the Execution Environment specified in the Install or Update command, it SHOULD reject the operation with a 9023 (Unknown Execution Environment) fault code.
//    If the device determines that the Deployment Unit being installed does not match either the Execution Environment specified or any Execution Environment on the device, it SHOULD reject the operation with a 9025 (Deployment Unit to Execution Environment Mismatch) fault code
//    If the device detects that the Deployment Unit being installed already has the same version as one already installed on the same Execution Environment, it SHOULD reject the operation with a 9026 (Duplicate Deployment Unit) fault code.
//    If the device detects that that there are no more system resources (disk space, memory, etc.) to perform the Install or Update of a Deployment Unit, it SHOULD reject the operation with a 9027 (System Resources Exceeded) fault code.
//    If a requested operation attempts to alter the State of a Deployment Unit in a manner that conflicts with the Deployment Unit State Machine Diagram [Appendix I "Software Module Management"/TR-369], it SHOULD reject the operation with a 9029 (Invalid Deployment Unit State) fault code.
//    If a requested operation attempts to Uninstall a DU that caused an EE to come into existence, where that EE has at least 1 installed DU or at least 1 child EE, then the device SHOULD reject the operation with a 9029 (Invalid Deployment Unit State) fault code.

typedef enum _tr181_fault_type {
    tr181_fault_ok = 0,
    tr181_fault_request_denied = 7002,
    tr181_fault_invalid_arguments = 7004,
    tr181_fault_unknown_exec_env = 7223,
    tr181_fault_du_to_ee_mismatch = 7225,
    tr181_fault_duplicate_du = 7226,
    tr181_fault_system_resources_exceeded = 7227,
    tr181_fault_invalid_du_state = 7229,
    tr181_fault_unavailable_role = 7032
} tr181_fault_type_t;

const char* tr181_fault_type_to_string(tr181_fault_type_t err);

#ifdef __cplusplus
}
#endif

#endif // __CTHULHU_ERROR_H__
