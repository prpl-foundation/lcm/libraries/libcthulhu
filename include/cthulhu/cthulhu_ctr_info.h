/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__CTHULHU_CTR_INFO_H__)
#define __CTHULHU_CTR_INFO_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <sys/types.h>
#include <amxc/amxc_llist.h>

typedef enum _cthulhu_ctr_status {      // LXC        crun
    cthulhu_ctr_status_unknown = 0,     // -          -
    cthulhu_ctr_status_ghost,           // -          -
    cthulhu_ctr_status_created,         // -          created
    cthulhu_ctr_status_stopped,         // STOPPED    stopped
    cthulhu_ctr_status_starting,        // STARTING   -
    cthulhu_ctr_status_running,         // RUNNING    running
    cthulhu_ctr_status_stopping,        // STOPPING   -
    cthulhu_ctr_status_restarting,      // -          -
    cthulhu_ctr_status_aborting,        // ABORTING   -
    cthulhu_ctr_status_freezing,        // FREEZING   -
    cthulhu_ctr_status_frozen,          // FROZEN     paused
    cthulhu_ctr_status_thawed,          // THAWED     -
    cthulhu_ctr_status_removed,
    cthulhu_ctr_status_last             // delimiter of enum
} cthulhu_ctr_status_t;

typedef struct _cthulhu_ctr_info_itf {
    char* name;         ///> The interface name
    amxc_llist_t ips;   ///> The ip addresses associated with this interface
    amxc_llist_it_t it;
} cthulhu_ctr_info_itf_t;

int cthulhu_ctr_info_itf_new(cthulhu_ctr_info_itf_t** itf);
void cthulhu_ctr_info_itf_delete(cthulhu_ctr_info_itf_t** itf);

int cthulhu_ctr_info_itf_init(cthulhu_ctr_info_itf_t* const itf);
void cthulhu_ctr_info_itf_clean(cthulhu_ctr_info_itf_t* const itf);
int cthulhu_ctr_info_itf_copy(cthulhu_ctr_info_itf_t* const dest, const cthulhu_ctr_info_itf_t* const src);

void cthulhu_interfaces_list_it_free(amxc_llist_it_t* it);

typedef struct _cthulhu_ctr_info {
    char* ctr_id;
    pid_t pid;
    cthulhu_ctr_status_t status;
    char* bundle;
    char* bundle_version;
    char* sb_id;
    char* created;
    char* owner;
    amxc_llist_t interfaces; //> a list of cthulhu_ctr_info_itf_t
} cthulhu_ctr_info_t;

int cthulhu_ctr_info_new(cthulhu_ctr_info_t** info);
void cthulhu_ctr_info_delete(cthulhu_ctr_info_t** info);

int cthulhu_ctr_info_init(cthulhu_ctr_info_t* const info);
void cthulhu_ctr_info_clean(cthulhu_ctr_info_t* const info);
void cthulhu_ctr_info_reset(cthulhu_ctr_info_t* const info);
int cthulhu_ctr_info_copy(cthulhu_ctr_info_t* const dest, const cthulhu_ctr_info_t* const src);

const char* cthulhu_ctr_status_to_string(cthulhu_ctr_status_t status);
cthulhu_ctr_status_t cthulhu_string_to_ctr_status(const char* status);

#ifdef __cplusplus
}
#endif

#endif // __CTHULHU_CTR_INFO_H__



