/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <cthulhu/cthulhu_sb_data.h>

#include <cthulhu_assert.h>
#include "cthulhu_priv.h"

int cthulhu_sb_data_new(cthulhu_sb_data_t** data) {
    int retval = -1;
    when_null(data, exit);

    *data = (cthulhu_sb_data_t*) calloc(1, sizeof(cthulhu_sb_data_t));
    when_null(*data, exit);

    // no need to check the return value of the following function
    // it can not fail.
    // the reason it returns an error code is for API consistency
    cthulhu_sb_data_init(*data);
    retval = 0;

exit:
    return retval;
}

void cthulhu_sb_data_delete(cthulhu_sb_data_t** data) {
    when_null(data, exit);

    cthulhu_sb_data_clean(*data);

    free_null(*data);
exit:
    return;
}

int cthulhu_sb_data_init(cthulhu_sb_data_t* const data) {
    int retval = -1;
    when_null(data, exit);
    data->sb_id = NULL;
    data->ns_pid = -1;
    data->ns_inherrit_uts = false;
    data->ns_inherrit_net = false;
    data->ns_inherrit_user = false;
    data->ns_inherrit_mnt = false;
    retval = 0;
exit:
    return retval;
}
void cthulhu_sb_data_clean(cthulhu_sb_data_t* const data) {
    when_null(data, exit);

    free_null(data->sb_id);
    cthulhu_sb_data_init(data);
exit:
    return;
}

void cthulhu_sb_data_reset(cthulhu_sb_data_t* const data) {
    cthulhu_sb_data_clean(data);
}


