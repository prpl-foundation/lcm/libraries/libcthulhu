/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE 1 // to make strdup work

#include <stdlib.h>
#include <string.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_string.h>
#include <amxc/amxc_utils.h>

#include <cthulhu/cthulhu_ctr_info.h>

#include <cthulhu_assert.h>
#include "cthulhu_priv.h"

#define strdup_null(x) x ? strdup(x) : NULL

static const char* cthulhu_ctr_status_names[] = {
    "Unknown",
    "Ghost",
    "Created",
    "Stopped",
    "Starting",
    "Running",
    "Stopping",
    "Restarting",
    "Aborting",
    "Freezing",
    "Frozen",
    "Thawed"
};

/************************/
/* cthulhu_ctr_info_itf */
/************************/

int cthulhu_ctr_info_itf_new(cthulhu_ctr_info_itf_t** itf) {
    int retval = -1;
    when_null(itf, exit);

    *itf = (cthulhu_ctr_info_itf_t*) calloc(1, sizeof(cthulhu_ctr_info_itf_t));
    when_null(*itf, exit);

    cthulhu_ctr_info_itf_init(*itf);
    retval = 0;

exit:
    return retval;
}

void cthulhu_ctr_info_itf_delete(cthulhu_ctr_info_itf_t** itf) {
    when_null(itf, exit);

    cthulhu_ctr_info_itf_clean(*itf);

    free_null(*itf);
exit:
    return;
}

int cthulhu_ctr_info_itf_init(cthulhu_ctr_info_itf_t* const itf) {
    int retval = -1;
    when_null(itf, exit);
    itf->name = NULL;
    amxc_llist_init(&itf->ips);
    amxc_llist_it_init(&itf->it);

    retval = 0;
exit:
    return retval;
}

void cthulhu_ctr_info_itf_clean(cthulhu_ctr_info_itf_t* const itf) {
    when_null(itf, exit);

    free_null(itf->name);
    amxc_llist_clean(&itf->ips, amxc_string_list_it_free);

exit:
    return;
}

int cthulhu_ctr_info_itf_copy(cthulhu_ctr_info_itf_t* const dest, const cthulhu_ctr_info_itf_t* const src) {
    int retval = -1;
    when_null(dest, exit);
    when_null(src, exit);

    // reset destination variant
    cthulhu_ctr_info_itf_clean(dest);

    dest->name = strdup_null(src->name);
    amxc_llist_for_each(it, (&src->ips)) {
        amxc_string_t* ip = amxc_string_from_llist_it(it);
        amxc_llist_add_string(&dest->ips, ip->buffer);
    }

    retval = 0;
exit:
    return retval;
}

void cthulhu_interfaces_list_it_free(amxc_llist_it_t* it) {
    cthulhu_ctr_info_itf_t* itf = amxc_llist_it_get_data(it, cthulhu_ctr_info_itf_t, it);
    cthulhu_ctr_info_itf_delete(&itf);
}

/********************/
/* cthulhu_ctr_info */
/********************/
int cthulhu_ctr_info_new(cthulhu_ctr_info_t** info) {
    int retval = -1;
    when_null(info, exit);

    *info = (cthulhu_ctr_info_t*) calloc(1, sizeof(cthulhu_ctr_info_t));
    when_null(*info, exit);

    // no need to check the return value of the following function
    // it can not fail.
    // the reason it returns an error code is for API consistency
    cthulhu_ctr_info_init(*info);
    retval = 0;

exit:
    return retval;
}

void cthulhu_ctr_info_delete(cthulhu_ctr_info_t** info) {
    when_null(info, exit);

    cthulhu_ctr_info_clean(*info);

    free_null(*info);
exit:
    return;
}

int cthulhu_ctr_info_init(cthulhu_ctr_info_t* const info) {
    int retval = -1;
    when_null(info, exit);
    info->ctr_id = NULL;
    info->pid = -1;
    info->status = cthulhu_ctr_status_unknown;
    info->bundle = NULL;
    info->bundle_version = NULL;
    info->sb_id = NULL;
    info->created = NULL;
    info->owner = NULL;
    amxc_llist_init(&info->interfaces);

    retval = 0;
exit:
    return retval;
}

int cthulhu_ctr_info_copy(cthulhu_ctr_info_t* const dest, const cthulhu_ctr_info_t* const src) {
    int retval = -1;
    when_null(dest, exit);
    when_null(src, exit);

    // reset destinationn variant
    cthulhu_ctr_info_clean(dest);
    // get the type function pointers of the src
    dest->ctr_id = strdup_null(src->ctr_id);
    dest->pid = src->pid;
    dest->status = src->status;
    dest->bundle = strdup_null(src->bundle);
    dest->bundle_version = strdup_null(src->bundle_version);
    dest->sb_id = strdup_null(src->sb_id);
    dest->created = strdup_null(src->created);
    dest->owner = strdup_null(src->owner);
    cthulhu_ctr_info_itf_t* src_itf = NULL;
    amxc_llist_for_each(it, (&src->interfaces)) {
        src_itf = amxc_llist_it_get_data(it, cthulhu_ctr_info_itf_t, it);
        cthulhu_ctr_info_itf_t* dest_itf = NULL;
        cthulhu_ctr_info_itf_new(&dest_itf);
        when_null(dest_itf, exit);
        cthulhu_ctr_info_itf_copy(dest_itf, src_itf);
        amxc_llist_append(&dest->interfaces, &dest_itf->it);
    }

exit:
    return retval;
}

void cthulhu_ctr_info_clean(cthulhu_ctr_info_t* const info) {
    when_null(info, exit);

    free_null(info->ctr_id);
    info->pid = -1;
    info->status = cthulhu_ctr_status_unknown;
    free_null(info->bundle);
    free_null(info->bundle_version);
    free_null(info->sb_id);
    free_null(info->created);
    free_null(info->owner);
    amxc_llist_clean(&info->interfaces, cthulhu_interfaces_list_it_free);

exit:
    return;
}

void cthulhu_ctr_info_reset(cthulhu_ctr_info_t* const info) {
    cthulhu_ctr_info_clean(info);
}

const char* cthulhu_ctr_status_to_string(cthulhu_ctr_status_t status) {
    if((status >= 0) && (status < cthulhu_ctr_status_last)) {
        return cthulhu_ctr_status_names[status];
    } else {
        return NULL;
    }
}

cthulhu_ctr_status_t cthulhu_string_to_ctr_status(const char* status_string) {
    int status_index = (int) cthulhu_ctr_status_unknown;
    cthulhu_ctr_status_t status = cthulhu_ctr_status_unknown;
    if(!status_string) {
        return cthulhu_ctr_status_unknown;
    }
    while(status < cthulhu_ctr_status_last) {
        if(strncmp(cthulhu_ctr_status_names[status], status_string,
                   strlen(cthulhu_ctr_status_names[status])) == 0) {
            return status;
        }
        status = (cthulhu_ctr_status_t)++ status_index;
    }
    return cthulhu_ctr_status_unknown;
}

